package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"htmlpdf/constant"
	"htmlpdf/templates"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"text/template"

	gopdf "github.com/SebastiaanKlippert/go-wkhtmltopdf"
)

func main() {
	fmt.Println("Start Generate PDF!")

	GenerateHTML()

	ExampleNewPDFGenerator()

}

func GenerateHTML() {

	// Open our jsonFile
	jsonFile, err := os.Open(constant.INPUT_JSON_PATH + "/" + constant.JSON_FILE_NAME)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened data.json")

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	var event map[string]interface{}
	dat, err := ioutil.ReadAll(jsonFile)
	err = json.Unmarshal(dat, &event)
	if err != nil {
		panic(err)
	}

	fmt.Println(AddTemplate(templates.Template2, event))
}

func AddTemplate(a string, b map[string]interface{}) string {
	fmap := template.FuncMap{
		"length": Length,
	}
	tmpl := template.Must(template.New("email.tmpl").Funcs(fmap).Parse(a))
	buf := &bytes.Buffer{}
	err := tmpl.Execute(buf, b)
	if err != nil {
		panic(err)
	}
	s := buf.String()

	err = ioutil.WriteFile(constant.OUTPUT_HTML_PATH+"/"+constant.HTML_FILE_NAME, []byte(s), 0755)
	return s
}

func Length(a interface{}) float64 {
	return float64(reflect.ValueOf(a).Len())
}

func ExampleNewPDFGenerator() {

	// Create new PDF generator
	pdfg, err := gopdf.NewPDFGenerator()
	if err != nil {
		log.Fatal(err)
	}

	// Set global options
	pdfg.Dpi.Set(300)
	pdfg.Grayscale.Set(false)

	// Create a new input page from an URL
	// page := gopdf.NewPage("https://www.google.com/")
	// page := gopdf.NewPage("../data/template/pdf-template.html")
	page := gopdf.NewPage(constant.OUTPUT_HTML_PATH + "/" + constant.HTML_FILE_NAME)

	// Set options for this page
	page.EnableLocalFileAccess.Set(true)
	// page.UserStyleSheet.Set("../data/template/stylesheet.css")
	// page.FooterRight.Set("[page]")

	// Add to document
	pdfg.AddPage(page)

	// Create PDF document in internal buffer
	err = pdfg.Create()
	if err != nil {
		log.Fatal(err)
	}

	// Write buffer contents to file on disk
	err = pdfg.WriteFile(constant.OUTPUT_PDF_PATH + "/" + constant.PDF_FILE_NAME)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Done!")
	// Output: Done

}
