package templates

const Template1 = `
<!DOCTYPE html>
	<html>
	  <head>
		<!-- Required meta tags -->
		<meta charset="utf-8" />
		<meta
		  name="viewport"
		  content="width=device-width, initial-scale=1, shrink-to-fit=no"
		/>
	</head>
	<body>
		<p>Series of anomalies found in {{.country}}, {{.city}}</p>
	  	<p>Hello</p>
		<p>This is an automated notification in response to a global time travel event. The
		<b>singularity in {{.country}}, {{.city}}</b> detected anomalies at <b>{{.lastModified}}</b>.</p>
		<p>Singularity power source: {{.source}}</p>
		<p>Supplier: {{.supplier}}</p>
		<p>Portal: {{.portal}}</p>
		<p>A total of <b>{{.totalAnomalies}}</b> anomalies have been found:</p>
		<table>
		{{- range .anomalies -}}
		{{if .character -}}
		<tr><td>Anomaly {{.record}}: {{.character}} has travelled from {{.origin_year}} to {{.destination_year}}</td></tr>
		{{- else }}
		  <tr><td>Anomaly {{.record}} - {{.message}}</td></tr>
		{{- end}}
	  {{- end}}
	  </table>
	  {{if gt .totalAnomalies (length .anomalies) -}}
		<p><i><b>Note:</b> only up to the first {{length .anomalies}} anomalies have been displayed, a total of {{.totalAnomalies}} anomalies have been identified.</i></p>
		{{- end}}
	  <p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>
	  </body>
	</html>
`
