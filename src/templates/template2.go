package templates

const Template2 = `
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta
  name="viewport"
  content="width=device-width, initial-scale=1, shrink-to-fit=no"
/>
<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous" />
<link rel="stylesheet" href="../../data/template/stylesheet.css">
</head>

<body>
  <div class="container up-20">
    <div class="row">
      <div class="col-3">
        <img src="../../src/images/g2.png" width="50%" />
      </div>
      <div class="col d-flex" style="margin-top: 15px; margin-left: 20px;">
        <p class="align-self-center text-sm">
          xxx
          <br />
          xxx
          <br />
          xxx
          <br />
          xxx
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-6 d-flex test-ja">
        <div class="align-self-center">
          <div class="row v-padding-2">
            <div class="col-4">
              xxx
            </div>
            <div class="col-8">
              xxx
            </div>
          </div>
          <div class="row v-padding-2">
            <div class="col-4">
              ADDRESS
            </div>
            <div class="col-8">
              111 MBK Building<br />
              กรุงเทพมหานคร
            </div>
          </div>
          <div class="row v-padding-2">
            <div class="col-4">
              xxx
            </div>
            <div class="col-3">
              xxx
            </div>
            <div class="col-5">
              xxx
            </div>
          </div>
          <div class="row v-padding-2">
            <div class="col-4">
              xxx
            </div>
            <div class="col-8">
              xxx
            </div>
          </div>
        </div>
      </div>
      <div class="col-6 text-sm">
        <p class="small-header-text">
          For Title ...
        </p>
        <div class="row v-padding-2">
          <div class="col-4">xxx</div>
          <div class="col-4 before-colon padding-left-0 padding-right-0">&#160 xxx</div>
          <div class="col-4 padding-left-0">xxx</div>
        </div>
        <div class="row v-padding-2">
          <div class="col-4">xxx</div>
          <div class="col-8 before-colon padding-left-0">&#160 xxx</div>
        </div>
        <div class="row v-padding-2">
          <div class="col-4">xxx</div>
          <div class="col-8 before-colon padding-left-0">&#160 xxx</div>
        </div>
        <div class="row v-padding-2">
          <div class="col-4">xxx</div>
          <div class="col-8 before-colon padding-left-0">&#160 xxx</div>
        </div>
        <div class="row v-padding-2">
          <div class="row v-padding-2">
            <div class="col-12 padding-right-0">xxx :&#160&#160 xxx</div>
          </div>
        </div>
        <div class="row v-padding-2">
          <div class="col-4">ADDRESS</div>
          <div class="col-8 before-colon padding-left-0">
            444 xxx
            <br> &#160 xxx
            <br> &#160 xxx
          </div>
        </div>
        <div class="row v-padding-2">
          <div class="col-12">xxx &#160:&#160&#160 xxx</div>
        </div>
        <div class="row v-padding-2">
          <div class="col-12">xxx &#160:&#160&#160 xxx</div>
        </div>
      </div>
    </div>
    <br>
    <table class="table table-bordered border-dark">
      <thead>
        <th class="col-2 center-text v-padding-2" >
          xxx
        </th>
        <th class="col-2 center-text v-padding-2">
          xxx
        </th>
        <th class="col-2 center-text v-padding-2">
          xxx
        </th>
        <th class="col-2 center-text v-padding-2">
          xxx
        </th>
        <th class="col-2 center-text v-padding-2">
          xxx
        </th>
        <th class="col-2 center-text v-padding-2">
          xxx
        </th>
      </thead>
      <tbody>
		<!--##### MAXIUM 32 (+1) rows ######-->
		{{- range .cardTypeSummary -}}
        <tr class="B2kTable">
          <td class="col-2 center-text B2kTable-padding-col">
            <p class="up-20" style="display: inline">{{.cardType}}</p>
          </td>
          <td class="col-2 text-right B2kTable-padding-col">
		  {{.sdAmount}}
          </td>
          <td class="col-2 text-right B2kTable-padding-col">
		  {{.discountAmount}}
          </td>
          <td class="col-2 text-right B2kTable-padding-col">
		  {{.vatAmount}}
          </td>
          <td class="col-2 text-right B2kTable-padding-col">
		  {{.totalDiscountAmount}}
          </td>
          <td class="col-2 text-right B2kTable-padding-col">
		  {{.totalAmount}}
          </td>
        </tr>
		{{- end}}
        <tr>
          <td colspan="6" class="v-padding-0">
            <div class="row">
              <div class="col-12">
                xxx  &#160&#160 = &#160&#160&#160&#160&#160&#160&#160&#160&#160&#160 Total DISCOUNT+VAT
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="6" class="v-padding-0">
            <div class="row">
              <div class="col-6">
                xxx &#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160&#160 = &#160&#160&#160&#160&#160&#160&#160&#160&#160&#160
                {{.summary.whtAmount}}
              </div>
              <div class="col-6">
                xxx &#160&#160 = &#160&#160&#160&#160&#160&#160&#160&#160&#160&#160
                xxx,xxx.xx
              </div>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
    <div class="row d-flex">
      <div class="col-9 border border-dark align-self-start " style="padding: 10px; ">
        xxx <span>xxx</span>
        xxxxxx
        xxxxxxxxx
      </div>
      <div class="col-3">
        <img src="../../src/images/g2.png" width="40%" />
      </div>
    </div>
    <div class="row" style="margin-top: 25px; margin-right: 11px;">
	  <p>
	  xxx <br />
        xxxx
        xxxx
        xxxx
        <br />
      xxx</p>
    </div>

    <p class="page-footer">หน้าที่ 1/X</p>

  </div>
</body>

</html>
`
