package constant

const HTML_FILE_NAME = "template_applied_json.html"
const PDF_FILE_NAME = "output.pdf"
const JSON_FILE_NAME = "JSON_example_POC.json"

const INPUT_JSON_PATH = "/Users/xxx/Documents/dynamichtmlgenpdf/data/inbound"

const OUTPUT_HTML_PATH = "/Users/xxx/Documents/dynamichtmlgenpdf/data/inbound"
const OUTPUT_PDF_PATH = "/Users/xxx/Documents/dynamichtmlgenpdf/data/outbound"

const IMAGE_PATH = "/Users/xxx/Documents/dynamichtmlgenpdf/src/images"

const CSS_STYLE_PATH = "/Users/xxx/Documents/dynamichtmlgenpdf/data/template"
