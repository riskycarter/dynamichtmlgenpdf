## Coding language : golang
## Objective : to generate HTML file from json and generate output file to PDF format

# Prerequisite

> wkhtmltopdf --> https://wkhtmltopdf.org/downloads.html

# Encrypt password PDF

> qpdf --> https://sourceforge.net/projects/qpdf/


``` bash
# command to set password
qpdf --encrypt [password] [re-password] 40 -- [input.pdf] [output.pdf]

```